import java.util.*;        
public class GoldPoint {
public static void main(String[] args){
	GoldPoint gd=new GoldPoint();
	gd.goldPoint();
	}
	int n;
	Object [] m=new Object[50];              //定义Object数组，里面可以放任何类。用来存放输入的数值，Object避免数据类型错误。 
		int peopleNum;			//参加的人数
		int time;				//进行轮数
	public void goldPoint(){
		HashMap<String,Double> inputMap=new HashMap<String,Double>();        //存入输入数值
		HashMap<String,Double> scoreMap=new HashMap<String,Double>();        //存入分数
		String name="";
		Double inputScore;
		Scanner scan=new Scanner(System.in);                    //参数对象是从控制台读入的数据流
		System.out.println("*****黄金点游戏*****");
		System.out.println("输入参加游戏的人数：");
		peopleNum=scan.nextInt();
		n=peopleNum;
 		System.out.println("输入游戏进行的轮数：");
		time=scan.nextInt();
		System.out.println("第一轮游戏开始！");
		Double sum=0.0;
   		Double G=0.0;
    		for(int i=0;i<peopleNum;i++){
        		System.out.println("请输入第"+(i+1)+"名玩家姓名：");
		        name=scan.next();
		        System.out.println("请输入第一轮输入的数值：");
		        inputScore=scan.nextDouble();
				m[i]=inputScore;                     //将输入的数值存放到数组中
		        inputMap.put(name, inputScore);
 		        scoreMap.put(name,(double) 0);//初始化scoreMap
		        sum+=inputScore;
			}
		G=sum/peopleNum*0.618;
	  	System.out.println("G="+G);
  		this.findWinner(inputMap, scoreMap, G);
		this.show(scoreMap);
		System.out.println("第一轮结束");
		
         //轮数循环的开始
		for(int i=0;i<time-1;i++){
            	sum=0.0;
	        System.out.println("第"+(i+2)+"轮游戏开始！");
	        Iterator iter = inputMap.entrySet().iterator();
			int temp=0;
	        while (iter.hasNext()) {
                	Map.Entry entry0 = (Map.Entry) iter.next();
	                String key = (String) entry0.getKey();
	                System.out.println(key+"输入第"+(i+2)+"轮输入的数值：");
                	Double score =scan.nextDouble();
					m[temp]=score;
	                inputMap.put(key, score);           //替换掉以前的分数
                	sum+=score;
					temp++;
	        	}
	        G=sum/peopleNum*0.618;
		System.out.println("G="+G);
	        this.findWinner(inputMap, scoreMap, G);
	        this.show(scoreMap);
	        System.out.println("第"+(i+2)+"轮结束");
   		} 
    		System.out.println("游戏结束"); 

	}

	//找出每次分数最接近黄金点的 和最远的 最接近的加N分 最远的减2分 其余加零分（可能有相同的）

	public void findWinner(HashMap<String,Double> inputMap,HashMap<String,Double> scoreMap,Double G){    
    		Double temp;          //定义新变量，用来比较最近的一个数值
    		Double temp0;         //定义新变量，用来比较最远的一的数值
		List<String> latest=new ArrayList<String>();
		List<String> farthest=new ArrayList<String>();
		Iterator iter = inputMap.entrySet().iterator();
		Map.Entry entry = (Map.Entry) iter.next();	
    		Double input = (Double) entry.getValue();
		String key0 = (String) entry.getKey();
		latest.add(key0);
    		farthest.add(key0);
    		iter.hasNext();
    		temp0=temp=Math.abs(G-input);
		//遍历map
		while (iter.hasNext()) {    
		        entry = (Map.Entry) iter.next();
		        String key = (String) entry.getKey();
        		input = (Double) entry.getValue();
		        Double temp1=Math.abs(G-input);
	        	if(temp>temp1){//寻找最近的数值
		        	temp=temp1;
	                	latest.clear();
	                	latest.add(key);
	         	}
			else if(temp==temp1){
		        	latest.add(key);
        		}
	        	if(temp0<temp1){//寻找最远的数值
		        	temp0=temp1;
		        	farthest.clear();
            			farthest.add(key);
			}
	       		else if(temp0==temp1){      //有可能出现离G值相同的数值
		        	farthest.add(key);
	        	}
		}
    		//实现加分
    		iter = scoreMap.entrySet().iterator();	
	    	while (iter.hasNext()) {
	        Map.Entry entry0 = (Map.Entry) iter.next();
	        String key = (String) entry0.getKey();
	        Double score =(Double) entry0.getValue();
	        if(this.containList(key, latest)){
	            score=score+n;              //分数最接近黄金点的加N分
	            scoreMap.put(key, score);
	            }
	        if(this.containList(key, farthest)){
	            score=score-2;               //分数最最远的减2分
	            scoreMap.put(key, score);
	            }
        	}
	}
	public boolean containList(String str,List<String> list){
    		for(int i=0;i<list.size();i++){
	        	if(str.equals(list.get(i))){
	            	return true;
	        	}
		}
    		return false;
		}
	public void show(HashMap<String,Double> scoreMap){
			for (int i=0;i<peopleNum;i++){
			     
				System.out.println("第"+(i+1)+"个人输入的数值："+m[i]);   //循环依次输出每人个猜的数值
			}
			System.out.println("最终得分情况：");
		Iterator iter = scoreMap.entrySet().iterator();
		while (iter.hasNext()) {
	        	Map.Entry entry0 = (Map.Entry) iter.next();
	        	String key = (String) entry0.getKey();
	        	Double score =(Double) entry0.getValue();
	        	System.out.println(key+"："+score);
    		}

	} 

}